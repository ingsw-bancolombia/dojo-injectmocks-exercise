package co.com.bancolombia.dojoinjectmocksexercise.exercises;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import co.com.bancolombia.dojoinjectmocksexercise.fakelibraries.AmazonCloudWatch;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class UsingAmazonCloudWatchTest {

    @Mock
    private AmazonCloudWatch awsMock;

    @InjectMocks
    private ExampleUsingAmazonCloudWatch exampleUsingAmazonCloudWatch;

    @Test
    public void testPutAlarmCorrectly () {
        when(awsMock.putMetricAlarm(any(String.class))).thenReturn("Mock putMetricAlarm!");
        exampleUsingAmazonCloudWatch.putAlarmCloudWatchService();
        verify(awsMock, times(1)).putMetricAlarm(any(String.class));
    }
}
